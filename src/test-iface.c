/*
 * Copyright (C) 2003 Sun Microsystems, Inc.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.

 * Authors:
 *      Mark McLoughlin <mark@skynet.ie>
 */

#include <config.h>

#include <gtk/gtk.h>

#include "netstatus-iface.h"

static void
iface_state_changed (NetstatusIface *iface)
{
  int in_packets = 0, out_packets = 0;

  g_print ("State changed to %s\n", netstatus_get_state_string (netstatus_iface_get_state (iface)));

  netstatus_iface_get_packet_statistics (iface, &in_packets, &out_packets);
  g_print ("In: %d, Out: %d\n", in_packets, out_packets);
}

int
main (int argc, char **argv)
{
  NetstatusIface *iface;
  GSList         *interfaces, *l;

  gtk_init (&argc, &argv);

  netstatus_setup_debug_flags ();

  interfaces = netstatus_list_interface_names (NULL);
  for (l = interfaces; l; l = l->next)
    {
      g_print ("%s\n", (char *) l->data);
      g_free (l->data);
    }
  g_slist_free (interfaces);
  
  iface = netstatus_iface_new ("eth0");

  g_print ("iface name: %s\n", netstatus_iface_get_name (iface));

  g_signal_connect (iface, "notify::state",
		    G_CALLBACK (iface_state_changed), NULL);
  
  gtk_main ();
  
  g_object_unref (iface);

  return 0;
}
